package model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.Instant;

@Converter(autoApply = true)
public class InstantConverter implements AttributeConverter<Instant, Timestamp> {
	public Instant convertToEntityAttribute(Timestamp date) {
		if (date == null) {return null;}
		return date.toInstant();
	}

	public Timestamp convertToDatabaseColumn(Instant instant) {
		if (instant == null) {return null;}
		return Timestamp.from(instant);
	}
}
