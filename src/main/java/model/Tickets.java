package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Tickets {
	@Embeddable
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Key{
		@Column(name = "event", nullable = false, insertable = false, updatable = false)
		long event;
		@Column(name = "id", nullable = false)
		@Getter
		int id;
	}
	@Getter@Setter @EmbeddedId Key key;

	@ManyToOne
	@JoinColumn(name="user", referencedColumnName = "id", nullable = false)
	@Getter@Setter
	User user;

	@ManyToOne
	@JoinColumn(name = "event", referencedColumnName = "id", insertable = true, updatable = false, nullable = false)
	@Getter@Setter
	Event event;

	@Column(name = "amount", nullable = false)
	@Getter@Setter
	long amount;
}
