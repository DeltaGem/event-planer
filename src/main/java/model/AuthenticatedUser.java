package model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.Getter;
import lombok.Setter;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="id")
public class AuthenticatedUser extends User {
	@Getter@Setter
	@Column(nullable=false, unique=true)
	String email;
	
	@Getter@Setter
	@Column(nullable=false)
	String password;
	
	@Getter@Setter
	@Column(nullable=false)
	Boolean manager;
}
