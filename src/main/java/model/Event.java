package model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.List;

@Entity
public class Event {
	@Getter @Setter
	@GeneratedValue
	@Id long id;
			
	@Column(nullable=false)
	@Getter @Setter
	String name;
	
	@Column(nullable=false)
	@Getter @Setter
	String teaser = "[default teaser]";
	
	@Column(nullable=false)
	@Getter @Setter
	String description = "[default description]";
	
	@Column(nullable=false)
	@Getter @Setter
	Instant startDate;

	@Getter @Setter
	boolean released;
	
	@Column(nullable=false)
	@Getter @Setter
	Instant endDate;

	@Column(nullable=false)
	@Getter @Setter
	long capacity;
	
	@ManyToOne
	@JoinColumn(name = "organizer", referencedColumnName = "id", nullable = false)
	@Getter@Setter
	AuthenticatedUser organizer;

	@OneToMany(mappedBy = "event")
	@Getter@Setter
	List<Tickets> tickets;
}
