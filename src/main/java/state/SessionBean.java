package state;
import lombok.Getter;
import lombok.Setter;
import model.AuthenticatedUser;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class SessionBean implements Serializable {
	@Getter@Setter
	private AuthenticatedUser currentUser = null;
	public boolean isLoggedIn() {return currentUser != null;}
	public void logout(){
		currentUser=null;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}
}
