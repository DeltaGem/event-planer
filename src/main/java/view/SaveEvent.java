package view;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import service.EventManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.time.Instant;

@Named
@RequestScoped
public class SaveEvent {
	@Getter@Setter private long id;
	@Getter@Setter private String name;
	@Getter@Setter private String teaser;
	@Getter@Setter private String description;
	@Getter@Setter private Instant startDate;
	@Getter@Setter private Instant endDate;
	@Getter@Setter private long capacity;

	@Inject EventManager eventManager;
	@Inject SessionBean session;

	@Transactional
	public String save() {
		if (id == 0) {
			val event = eventManager.createEvent(name, teaser, description, startDate, false, endDate, capacity, session.getCurrentUser());
			id = event.getId();
			return "created.xhtml";
		} else {
			eventManager.saveEvent(id, name, teaser, description, startDate, false, endDate, capacity, session.getCurrentUser());
			return "genericSuccess.xhtml";
		}
	}
}