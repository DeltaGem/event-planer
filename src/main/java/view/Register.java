package view;

import lombok.Getter;
import lombok.Setter;
import service.UserManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Register {
	@Getter@Setter
	String name;
	@Getter@Setter
	String email;
	@Getter@Setter
	String password;
	@Getter@Setter
	String passwordRepeat;
	@Getter@Setter
	boolean manager = false;
	@Inject
	UserManager userManager;
	@Inject
	SessionBean session;
	
	public String register(){
		session.setCurrentUser(userManager.register(name, password, email, manager));
		return "registerSuccess.xhtml";
	}

}
