package view;

import lombok.val;
import model.Event;
import service.EventManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class EventList {
	@Inject private EventManager eventmanager;
	@Inject private SessionBean session;
	@Inject @RequestParameters.Get private Map<String, String> getParams;

	public String getSearchWord() {
		val q = getParams.get("q");
		return q == null ? "" : q;
	}

	public boolean isOwnEvents() {return getParams.get("own") != null;}

	@Transactional
	public List<Event> getEvents() {
		val searchWord = getSearchWord();
		val user = session.getCurrentUser();
		return !isOwnEvents() ? eventmanager.searchAllEvents(searchWord, user) :
			user != null ? eventmanager.searchMyEvents(searchWord, user)
			: new ArrayList<Event>(0);
	}
	public boolean isErrorResponse() {return FacesContext.getCurrentInstance().isValidationFailed();}
}
