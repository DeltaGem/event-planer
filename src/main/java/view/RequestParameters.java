package view;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

@RequestScoped
public class RequestParameters {
	@Qualifier
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
	public @interface Get {}

	private FacesContext context = FacesContext.getCurrentInstance();

	@Produces @Get @RequestScoped
	public Map<String, String> getParameters() {return context.getExternalContext().getRequestParameterMap();}
}
