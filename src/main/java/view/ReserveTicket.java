package view;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.val;
import model.Event;
import service.EventManager;
import service.UserManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Named
@RequestScoped
public class ReserveTicket {
	@Getter@Setter private long event;
	@Getter@Setter private long amount;
	@Getter@Setter private String name;
	@Inject private EventManager eventManager;
	@Inject private SessionBean session;
	@Inject private EntityManager entityManager;
	@Inject private UserManager userManager;

	@Transactional
	public String reserve() {
		val user = session.getCurrentUser();
		boolean ergebnis = eventManager.book(eventManager.getEvent(event), amount, user == null ? userManager.createNonAuthenticatedUser(name) : user);
		if (!ergebnis) {
			FacesContext context=FacesContext.getCurrentInstance(); 
			context.getExternalContext().setResponseStatus(400);
			context.validationFailed();			
		}
		return "genericSuccess.xhtml";
	}

	public long getAvailableTickets(@NonNull Event ev) {return eventManager.availableTickets(ev);}
}
