package view;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import state.SessionBean;

@Named
@RequestScoped
public class Logout implements Serializable {

	@Inject
	SessionBean session;
	public void logout(){
		session.logout();
	}
}
