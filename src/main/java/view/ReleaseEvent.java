package view;

import lombok.NonNull;
import lombok.val;
import model.Event;
import model.Tickets;
import service.EventManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class ReleaseEvent {
	@Inject EventManager eventManager;
	@Inject SessionBean session;

	@Transactional
	public String release(Event event){
		if(isOrganizedByMe(event) && !event.isReleased()){
			eventManager.releaseEvent(event);
		}
		return "genericSuccess.xhtml";
	}
	public boolean isOrganizedByMe(@NonNull Event event){
		val user = session.getCurrentUser();
		if(user==null) {return false;}
		return user.getId() == event.getOrganizer().getId();
	}

	@Transactional
	public List<Tickets> getMyReservations(@NonNull Event ev) {
		if (isOrganizedByMe(ev)) {
			return eventManager.allTickets(ev);
		} else {
			val user = session.getCurrentUser();
			if(user==null){
				return new ArrayList<Tickets>(0);
			}
			return eventManager.ticketsForUser(ev, session.getCurrentUser());
		}
	}
}
