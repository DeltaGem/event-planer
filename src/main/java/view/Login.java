package view;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import service.UserManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class Login implements Serializable {
	@Getter@Setter
	String email;
	@Getter@Setter
	String password;
	@Inject
	UserManager userManager;
	@Inject
	SessionBean session;
	
	public void login(){
		val user = userManager.login(email, password);
		if(user == null){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ungültige Email- und Passwort-Kombination", ""));
			context.getExternalContext().setResponseStatus(400);
			FacesContext.getCurrentInstance().validationFailed();
		}else{
			session.setCurrentUser(user);
		}
	}
}
