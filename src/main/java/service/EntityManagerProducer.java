package service;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class EntityManagerProducer {
	@PersistenceContext(name = "h2")
	private EntityManager em;
	@Produces @RequestScoped EntityManager getEm() {return em;}
}
