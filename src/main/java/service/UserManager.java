package service;

import lombok.NonNull;
import lombok.val;
import model.AuthenticatedUser;
import model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

@ApplicationScoped
public class UserManager {
	@Inject private EntityManager entityManager;

	@Transactional public User createNonAuthenticatedUser(@NonNull String name) {
		val user = new User();
		user.setName(name);
		entityManager.persist(user);
		return user;
	}
	
	@Transactional
	public AuthenticatedUser register(@NonNull String name, @NonNull String password, @NonNull String email, boolean isManager ){
		AuthenticatedUser user = new AuthenticatedUser();
		user.setName(name);
		user.setPassword(password);
		user.setEmail(email);
		user.setManager(isManager);
		this.entityManager.persist(user);
		return user;
	}
	
	@Transactional
	public AuthenticatedUser login(@NonNull String email, @NonNull String password ){
		val query = entityManager.createQuery("Select u from AuthenticatedUser u where u.email=:email and u.password=:password", AuthenticatedUser.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		try{
			return query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}
	public boolean isEmailUsed(@NonNull String email){
		val query = entityManager.createQuery("Select u.email from AuthenticatedUser u where u.email=:email", String.class);
		query.setParameter("email", email);
		try{
			query.getSingleResult();
			return true;
		}catch(NoResultException e){
			return false;
		}
	}
	
}
