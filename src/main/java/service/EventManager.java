package service;

import lombok.NonNull;
import lombok.val;
import model.AuthenticatedUser;
import model.Event;
import model.Tickets;
import model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;

@ApplicationScoped
public class EventManager {
	@Inject private EntityManager entityManager;
	@Transactional
	public Event createEvent(@NonNull String name, @NonNull String teaser, @NonNull String description, @NonNull Instant startDate, boolean released, @NonNull Instant endDate, long capacity, @NonNull AuthenticatedUser organizer ){
		Event event=new Event();
		event.setName(name);
		event.setTeaser(teaser);
		event.setDescription(description);
		event.setStartDate(startDate);
		event.setReleased(false);
		event.setEndDate(endDate);
		event.setCapacity(capacity);
		event.setOrganizer(organizer);
		this.entityManager.persist(event);
		return event;
	}
	static class WrongOrganizer extends RuntimeException {}
	@Transactional
	public void saveEvent(long id, @NonNull String name, @NonNull String teaser, @NonNull String description, @NonNull Instant startDate, boolean released, @NonNull Instant endDate, long capacity, @NonNull AuthenticatedUser organizer ){
		val event = entityManager.find(Event.class, id);
		if (event.getOrganizer().getId() != organizer.getId()) {throw new WrongOrganizer();}
		event.setName(name);
		event.setTeaser(teaser);
		event.setDescription(description);
		event.setStartDate(startDate);
		event.setReleased(released);;
		event.setEndDate(endDate);
		event.setCapacity(capacity);
		this.entityManager.merge(event);
	}

	@Transactional public Event getEvent(long id) {return entityManager.find(Event.class, id);}

	@Transactional
	public List<Event> getAllEvents(){
		val query = entityManager.createQuery("Select e from Event e", Event.class);
		try{
			return query.getResultList();
		}catch(NoResultException e){
			return null;
		}
	}
	@Transactional
	public void releaseEvent(@NonNull Event event){
		event.setReleased(true);
		entityManager.merge(event);
	}
	private List<Event> fillAndSubmitSearch(@NonNull TypedQuery<Event> query, @NonNull String searchWord, AuthenticatedUser user) {
		query.setParameter("user", user);
		query.setParameter("name","%"+searchWord+"%");
		return query.getResultList();
	}
	@Transactional
	public List<Event> searchAllEvents(@NonNull String searchWord, AuthenticatedUser user) {
		return fillAndSubmitSearch(
			entityManager.createQuery("Select e from Event e where e.name LIKE :name and (e.released=true or e.organizer = :user)", Event.class),
			searchWord, user
		);
	}
	@Transactional
	public List<Event> searchMyEvents(@NonNull String name, @NonNull AuthenticatedUser user){
		return fillAndSubmitSearch(
			entityManager.createQuery("Select e from Event e where e.name LIKE :name and (e.organizer = :user or (exists(select t from Tickets t where t.user=:user) and e.released=true))", Event.class),
			name, user
		);
	}
	@Transactional public long availableTickets(@NonNull Event ev) {
		val query = entityManager.createQuery("Select sum(t.amount) from Tickets t where t.event=:ev", Long.class);
		query.setParameter("ev", ev);
		val sum = query.getSingleResult();
		return ev.getCapacity() - (sum == null ? 0 : sum);
	}
	@Transactional
	public boolean book(@NonNull Event event, long amount, @NonNull User user){
		long availableTickets = availableTickets(event);
		if(availableTickets >= amount){
			val query = entityManager.createQuery("select max(t.key.id) from Tickets t where t.event=:ev", Integer.class);
			query.setParameter("ev", event);
			val ticketId=query.getSingleResult();
			val tickets = new Tickets();
			tickets.setKey(new Tickets.Key(event.getId(), (ticketId == null ? 0 : ticketId+1)));
			tickets.setEvent(event);
			tickets.setUser(user);
			tickets.setAmount(amount);
			this.entityManager.persist(tickets);
			return true;
		}else{return false;}
	}

	@Transactional public List<Tickets> ticketsForUser(@NonNull Event ev, @NonNull User user) {
		val query = entityManager.createQuery("select t from Tickets t where t.event=:ev and t.user=:user", Tickets.class);
		query.setParameter("ev", ev);
		query.setParameter("user", user);
		return query.getResultList();
	}

	@Transactional public List<Tickets> allTickets(@NonNull Event ev) {
		val query = entityManager.createQuery("select t from Tickets t where t.event=:ev", Tickets.class);
		query.setParameter("ev", ev);
		return query.getResultList();
	}
}
