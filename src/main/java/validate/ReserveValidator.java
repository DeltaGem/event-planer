package validate;

import service.EventManager;
import state.SessionBean;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class ReserveValidator implements Validator {
	@Inject SessionBean session;
	@Inject EventManager eventManager;
	boolean error=false;
	List<FacesMessage> messages;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Map attributMap = ((UIInput)component).getAttributes();
		messages = new ArrayList<>();
		long amount = (long) value;
		if(amount==0){
			emitError("0 Tickets sind unsinn!",context);
		}
		if(session.getCurrentUser()==null){
			String name = ((UIInput)attributMap.get("notAuthenticatedUser")).getValue().toString();
			if(name.length()==0){
				emitError("Bitte anmelden oder einen Namen eintragen", context);
			}
		}
		if(error){
			throw new ValidatorException(messages);
		}
	}private void emitError(String text,FacesContext context){
		error=true;
		messages.add(new FacesMessage(text));
		context.getExternalContext().setResponseStatus(400);
	}

}
