package validate;

import service.UserManager;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Named
@RequestScoped
public class RegisterValidator implements Validator {
	private boolean error = false;
	List<FacesMessage> messages;
	@Inject
	UserManager userManager;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		messages = new ArrayList<>();
		Map attributMap = ((UIInput)component).getAttributes();
		String name = ((UIInput)attributMap.get("name")).getValue().toString();
		String email = ((UIInput)attributMap.get("email")).getValue().toString();
		String password = ((UIInput)attributMap.get("password")).getValue().toString();
		String passwordRepeat = value.toString();
		
		if(name.isEmpty()){
			emitError("Name darf nicht leer sein",context);
		}
		if(!email.matches("[^@]+@[^@]+")){
			emitError("Ungültige E-Mail",context);
		}
		if(userManager.isEmailUsed(email)){
			emitError("Email ist schon vorhanden", context);
		}
		if(!password.equals(passwordRepeat)){
			emitError("Passwörter stimmen nicht überein",context);
		}
		if(password.length() < 8){
			emitError("Passwort ist kürzer als 8 Zeichen",context);
		}
		if(error){
			throw new ValidatorException(messages);
		}
	}
	private void emitError(String text,FacesContext context){
		error=true;
		messages.add(new FacesMessage(text));
		context.getExternalContext().setResponseStatus(400);
	}

}
