package convert;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@FacesConverter(forClass = Instant.class)
public class UtcInstantConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			return Instant.from(ZonedDateTime.of(LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(value)), ZoneId.of("Z")));
		} catch (Exception e) {
			return Instant.from(ZonedDateTime.of(LocalDateTime.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(value)), ZoneId.of("Z")));
		}
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}
}
