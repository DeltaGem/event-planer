(function() {"use strict";
	function gotoUrl(url) {
		const old = location.path;
		location.href = url;
		if (location.path === old) {location.reload()}
	}
	function submitForm(form, params) {
		const data = [...form.elements].filter(el => el.type != 'checkbox' || el.checked).map(el => {
			if ({}.hasOwnProperty.call(params, el.dataset.param)) {
				return encodeURIComponent(el.name) + '=' + encodeURIComponent(params[el.dataset.param]);
			}
			return encodeURIComponent(el.name) + '=' + encodeURIComponent(el.value);
		}).join('&');
		return new Promise((resolve, reject) => {
			const xhr = makeXHR('POST', '', {
				'Content-Type': 'application/x-www-form-urlencoded'
			}, xhr => {
				if (xhr.status < 200 || xhr.status >= 300) {reject(xhr);}
				resolve(xhr.responseXML);
			});
			xhr.responseType = 'document';
			xhr.send(data);
		});
	}
	const getAncestorId = anc => {
		while (!anc.dataset.id) {anc = anc.parentElement;}
		return anc.dataset.id;
	}
	document.addEventListener('click', ev => {
		const t = ev.target;
		if (t.tagName === 'A' && t.origin === location.origin && t.pathname === location.pathname && t.search === location.search) {
			if (t.dataset.action === 'reserveEvent') {
				sessionStorage.setItem('reserve-eventId', getAncestorId(t));
			}
		} else if (t.matches('.modal.active')) {
			location.href='#';
		} else {
			let anc = t;
			while (anc && !anc.classList.contains('foldable')) {anc = anc.parentElement;}
			anc && anc.classList.toggle('folded');
		}
	});
	const handleErrors = form => xhr => {
		let msg;
		if (xhr.status === 400) {
			msg = fromJsonMl(['ul.errors', ...[...xhr.responseXML.querySelectorAll('li')].map(x => x.textContent).map(msg => ['li', msg])]);
		} else {
			msg = fromJsonMl(['p.errors', "Unbekannter Fehler trat auf"])
			console.error(xhr);
		}
		const old = form.querySelector('.errors');
		if (old) {
			old.parentNode.replaceChild(msg, old);
		} else {
			form.insertBefore(msg, form.firstChild);
		}
	};
	const submitWithErrors = (form, action) => {
		submitForm(form, {}).then(action, handleErrors(form));
	};
	const formHandler = action => ev => {
		submitWithErrors(ev.target, action)
		ev.stopPropagation();
		ev.preventDefault();
		return false;
	};
	const loginForm = document.getElementById('login');
	loginForm && loginForm.addEventListener('submit', formHandler(x => gotoUrl("index.jsf?own")));
	const registerForm = document.getElementById('register');
	registerForm && registerForm.addEventListener('submit', formHandler(x => gotoUrl("index.jsf?own")));
	const logoutForm = document.getElementById('logout');
	logoutForm && logoutForm.addEventListener('submit', formHandler(x => gotoUrl("index.jsf")));
	const checkPasswords = () => {
		registerPassword.setCustomValidity(registerPassword.value.length < 8
			? "Passwort muss mindestens 8 Zeichen lang sein"
			: ""
		);
		repeatPassword && repeatPassword.setCustomValidity(
			repeatPassword.value !== registerPassword.value
				? "Die Passwörter stimmen nicht überein"
				: ""
		);
	};
	const registerPassword = document.getElementById('register-password');
	registerPassword && registerPassword.addEventListener('input', checkPasswords);
	const repeatPassword = document.getElementById('repeat-password');
	repeatPassword && repeatPassword.addEventListener('input', checkPasswords);
	registerPassword && checkPasswords();

	document.addEventListener('submit', ev => {
		const form = ev.target;
		if (form.classList.contains('release-form')) {
			submitWithErrors(form, () => gotoUrl("#event" + getAncestorId(form)))
		}
		ev.preventDefault();
		ev.stopPropagation();
		return false;
	});

	const reserveLogin = document.getElementById('reserve-login');
	const reserveForm = document.getElementById('reserve');
	reserveLogin && reserveLogin.addEventListener('submit', function(ev) {
		submitForm(loginForm, {
			email: this.elements.email.value,
			password: this.elements.password.value
		}).then(() => location.reload(), handleErrors(reserveForm));
		ev.preventDefault();
		ev.stopPropagation();
		return false;
	});
	reserveForm.addEventListener('submit', formHandler(() => {
		if (logoutForm) {
			location.href = "index.jsf?own#event" + sessionStorage.getItem('reserve-eventId')
		} else {
			reserveForm.insertBefore(fromJsonMl(['.success', "Tickets auf den Namen '" + document.getElementById('reserve-name').value + "' erfolgreich bestellt"]), reserveForm.firstChild);
		}
	}))
	const reserveLogout = document.getElementById('reserve-logout');
	reserveLogout && reserveLogout.addEventListener('click', ev => submitForm(logoutForm, {}).then(() => location.reload(), handleErrors(reserveForm)));

	const saveEventForm = document.getElementById('save-form');
	saveEventForm && saveEventForm.addEventListener('submit', formHandler(xml => gotoUrl('index.jsf?own#event' + xml.documentElement.textContent)));

	const switchToElement = el => {
		while (el) {
			const id = el.id;
			if (id === 'reserve') {
				el.querySelector('#reserve-event-id').value = sessionStorage.getItem('reserve-eventId');
			}
			const cl = el.classList;
			if (cl.contains('modal')) {
				el.classList.add('active');
			} else if (el.matches('.tab-body > *')) {
				const active = el.parentElement.querySelector('.active');
				active && active.classList.remove('active');
				cl.add('active');
			} else if (cl.contains('tabs')) {
				const id = el.querySelector('.tab-body > .active').id;
				el.querySelector('a.active').classList.remove('active');
				el.querySelector('a[href$="#'+id+'"]').classList.add('active');
			} else if (cl.contains('foldable')) {
				cl.remove('folded');
			}
			el = el.parentElement;
		}
	};
	const hashChangeHandler = () => {
		if (location.hash === '') {
			[...document.querySelectorAll('.modal.active')].forEach(x => x.classList.remove('active'))
			return;
		}
		switchToElement(document.getElementById(location.hash.substring(1)))
	};
	window.addEventListener('hashchange', hashChangeHandler);
	hashChangeHandler();

	const saveForm = document.getElementById('save-form');
	const eventSaves = new WeakMap();
	const saveHandler = function(ev) {
		let anc = this;
		while (!anc.dataset.id) {anc = anc.parentElement;}
		const data = Object.create(null);
		data.id = anc.dataset.id;
		for (let field of [...anc.querySelectorAll('[data-field]')]) {
			data[field.dataset.field] = field.value || field.textContent;
		}
		const saveAction = () => submitForm(saveForm, data).then(() => this.classList.add('success')).catch(() => this.classList.add('error')).then(() => this.classList.remove('saving'));
		(eventSaves.get(this.dataset.field) || Promise.resolve()).then(saveAction);
	};
	for (let marker of [...document.querySelectorAll('.editable-marker')]) {
		const parent = marker.parentElement;
		for (let field of [...parent.querySelectorAll('[data-field]')]) {
			field.contentEditable = "true";
			field.addEventListener('blur', saveHandler);
		}
	}
})();
